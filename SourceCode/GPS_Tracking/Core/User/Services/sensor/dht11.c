/*
 * dht11.c
 *
 *  Created on: Apr 10, 2021
 *      Author: manht
 */
#include "dht11.h"
// ----------------------------------------------------------------------------
static void dht11_gpio_input()
{
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin = DHT11_IN;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(DHT11_Port, &GPIO_InitStruct);
}
// ----------------------------------------------------------------------------
static void dht11_gpio_output()
{
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin = DHT11_IN;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(DHT11_Port, &GPIO_InitStruct);
}
// ----------------------------------------------------------------------------
static void dht11_gpio_clk()
{
	__HAL_RCC_GPIOC_CLK_ENABLE();
}
// ----------------------------------------------------------------------------
static void dht11_set_gpio()
{
	HAL_GPIO_WritePin(DHT11_Port, DHT11_IN, SET);
}
// ----------------------------------------------------------------------------
static void dht11_reset_gpio()
{
	HAL_GPIO_WritePin(DHT11_Port, DHT11_IN, RESET);
}
// ----------------------------------------------------------------------------
static u8 dht11_read_input()
{
	return HAL_GPIO_ReadPin(DHT11_Port, DHT11_IN);
}
// ----------------------------------------------------------------------------
static void dht11_init()
{
	dht11_gpio_output();	//Set pin as OUTPUT;
	dht11_reset_gpio();		//Pull pin LOW;
	HAL_Delay(18);
	dht11_set_gpio();
	delay_us(20);
	dht11_gpio_input();		//set pin as input;
}
// ----------------------------------------------------------------------------
static int8_t dht11_check_response()
{
	int8_t res = 0;
	delay_us(40);
	if (!dht11_read_input())
	{
		delay_us(80);
		if (dht11_read_input()) res = 1;
		else return 0;
		while(dht11_read_input());
	}
	return res;
}
// ----------------------------------------------------------------------------
static uint8_t dht11_read_d8()
{
	uint8_t i=0,j;
	for(j=0;j<8;j++)
	{
		while (!dht11_read_input()); // wait for the pin to go high
		delay_us(40);
		if(!dht11_read_input())
		{
			i &= ~(1<<(7-j));   		// write 0
		}
		else i|= (1<<(7-j));  		// if the pin is high, write 1
		while (dht11_read_input()); // wait for the pin to go low
	}
	return i;
}
// ----------------------------------------------------------------------------
u8 dht11_read_data(dht11_t *dat)
{
	dht11_init();
	if (dht11_check_response())
	{
		dat->RH_Byte1 = dht11_read_d8();
		dat->RH_Byte2 = dht11_read_d8();
		dat->Temp_Byte1 = dht11_read_d8();
		dat->Temp_Byte2 = dht11_read_d8();
		dat->Check_sum = dht11_read_d8();
		if (dat->Check_sum == dat->RH_Byte1+dat->RH_Byte2+dat->Temp_Byte1+dat->Temp_Byte2) return 1;
		else return 0;
	}
	return 0;
}


