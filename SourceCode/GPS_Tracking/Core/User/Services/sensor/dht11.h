/*
 * dht11.h
 *
 *  Created on: Apr 10, 2021
 *      Author: manht
 */

#ifndef SENSOR_DHT11_H_
#define SENSOR_DHT11_H_
#include "main.h"
#include "delay/delay.h"
#define DHT11_IN	GPIO_PIN_0
#define DHT11_Port	GPIOC
#define u8 uint8_t
typedef struct dht11_t{
	u8 RH_Byte1;
	u8 RH_Byte2;
	u8 Temp_Byte1;
	u8 Temp_Byte2;
	u8 Check_sum;
}dht11_t;
u8 dht11_read_data(dht11_t *dat);
#endif /* SENSOR_DHT11_H_ */
