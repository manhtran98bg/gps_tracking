/*
 * gps.h
 *
 *  Created on: Dec 3, 2021
 *      Author: manht
 */

#ifndef USER_SERVICES_GPS_GPS_H_
#define USER_SERVICES_GPS_GPS_H_

#include "uart/uart.h"
#include "io/io.h"
#include "fifo.h"
#include "func.h"
#include "main.h"
#define GPS_BUFFER_MAX_SIZE 1024
#define GPS_DELAY	200
typedef enum{
	OFF,
	ON
}pwr_state;
//typedef struct gps{
//	pwr_state state : ON ;
//	UART_HandleTypeDef *huart;
//	void * UARTx;
//	uint32_t baudrate;
//}gps_t;
typedef enum {
	FUNC_INIT,
}gps_func_t;
typedef struct {
	int ddd;
	uint8_t mm;
	int mmmm;
	char dec_degree[16];
}location_t;
typedef struct {
	char id[7];
	struct tm time;
	char time_chr[11];
	char data_valid[2];
	location_t lat;
	char lat_dir[2];
	char lat_chr[10];
	location_t lon;
	char lon_dir[2];
	char lon_chr[11];
	double speed;
	char speed_chr[6];
}rmc_t;
typedef struct{
	pwr_state pwr_state ;
	uint8_t inited : 1;
	uint8_t gps_time_sync : 1;
	int8_t retry;
	uint8_t data_in_tmp;
	uint8_t nmea_start : 1;
	uint8_t response : 1;
	uint8_t nmea_received : 1;
	uint16_t data_in_index ;
	uint32_t data_in_last_time;
	uint32_t gps_wait_time;
	UART_HandleTypeDef *huart;
	void * UARTx;
	uint32_t baudrate;
	rmc_t	rmc;
	uint8_t data_in[GPS_BUFFER_MAX_SIZE];
	fifo_t	cbuffer;
	uint8_t nmea_buffer[256];
	uint16_t nmea_index;
	uint16_t nmea_size;
	gps_func_t func;
}gps_t;
void gps_init(gps_t *gps);
void gps_receive_data_it(gps_t *gps);
void gps_process_data(gps_t *gps);
#endif /* USER_SERVICES_GPS_GPS_H_ */
