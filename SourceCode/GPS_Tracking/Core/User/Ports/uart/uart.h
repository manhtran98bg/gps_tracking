/*
 * uart.h
 *
 *  Created on: Dec 3, 2021
 *      Author: manht
 */

#ifndef USER_PORTS_UART_UART_H_
#define USER_PORTS_UART_UART_H_


#include "main.h"

#define UART_DEBUG	huart5
#define UART_GPS	huart4
uint8_t uart_init(UART_HandleTypeDef *huart, USART_TypeDef *UARTx, uint32_t baudrate);
HAL_StatusTypeDef uart_transmit(UART_HandleTypeDef *huart, uint8_t *pData, uint16_t Size);
#endif /* USER_PORTS_UART_UART_H_ */
