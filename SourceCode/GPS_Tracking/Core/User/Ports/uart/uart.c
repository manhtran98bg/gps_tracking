/*
 * uart.c
 *
 *  Created on: Dec 3, 2021
 *      Author: manht
 */
#include "uart.h"

uint8_t uart_init(UART_HandleTypeDef *huart, USART_TypeDef *UARTx, uint32_t baudrate){
	huart->Instance = UARTx;
	huart->Init.BaudRate = baudrate;
	huart->Init.WordLength = UART_WORDLENGTH_8B;
	huart->Init.StopBits = UART_STOPBITS_1;
	huart->Init.Parity = UART_PARITY_NONE;
	huart->Init.Mode = UART_MODE_TX_RX;
	huart->Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart->Init.OverSampling = UART_OVERSAMPLING_16;
	HAL_UART_DeInit(huart);
	if (HAL_UART_Init(huart) != HAL_OK) return 0;
	else return 1;
}
HAL_StatusTypeDef uart_transmit(UART_HandleTypeDef *huart, uint8_t *pData, uint16_t Size){
	return HAL_UART_Transmit(huart, pData, Size, 100);
}

